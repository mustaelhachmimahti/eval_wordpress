<?php
/**
 * Menu
 *
 * @package     Menu
 * @author      Mustapha El Hachmi Mahti
 * @copyright   2022 Mustapha El Hachmi Mahti
 * @license     GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name: Menu
 * Version:     1.0.0
 * Author:      Mustapha El Hachmi Mahti
 */


function menu_nav( $atts ){
echo '<ul style="list-style-type: none;
margin: 0;
padding: 0;
overflow: hidden;
background-color: #333;
width: 100%">
<li style="  float: left;"><a style="  
display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;" href="http://localhost:8888/?page_id=41&preview=true">Accueil</a></li>
<li style="  float: left;"><a style="  
display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;" href="http://localhost:8888/sample-page/">Auteur</a></li>
<li style="  float: left;"><a style=" 
 display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;" href="http://localhost:8888/?page_id=34&preview=true">Annuaire</a></li>
<li style="float:right; background-color: #04AA6D;"><a style="  
display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;" class="active" href="#about">About</a></li>
</ul>';
}
add_shortcode( 'menu', 'menu_nav' );
