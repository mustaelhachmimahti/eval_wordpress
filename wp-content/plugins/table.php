<?php
/**
 * Table
 *
 * @package     Table
 * @author      Mustapha El Hachmi Mahti
 * @copyright   2022 Mustapha El Hachmi Mahti
 * @license     GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name: Table
 * Version:     1.0.0
 * Author:      Mustapha El Hachmi Mahti
 */

function annuaire( $atts ){

  $servername = "localhost";
  $database = "eval";
  $username = "root";
  $password = "root";
  
  // Create connection
  
  $conn = mysqli_connect($servername, $username, $password, $database);
  
  // Check connection
  
  if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
  }
  
  $res = $conn->query('SELECT * FROM wp_annuaire');
  

   $table = '<table class="center" style="margin-left: auto; 
   margin-right: auto; border: 1px solid black;">
     <tr>
     <th style="border: 1px solid black;">Id</th>
       <th style="border: 1px solid black;">Nom Entreprise</th>
       <th style="border: 1px solid black;">Localisation Entreprise</th>
       <th style="border: 1px solid black;">Prenom Contact</th>
       <th style="border: 1px solid black;">Nom Contact</th>
       <th style="border: 1px solid black;">Mail Contact</th>
     </tr>';

     while($r= $res->fetch_array(MYSQLI_ASSOC)){

      $table = $table . '<tr>
       <td style="border: 1px solid black;">'. $r["id"] .'</td>
       <td style="border: 1px solid black;">' . $r["nom_entreprise"] .'</td>
       <td style="border: 1px solid black;">'. $r["localisation_entreprise"] .'</td>
       <td style="border: 1px solid black;">'.$r["prenom_contact"] .'</td>
       <td style="border: 1px solid black;">'.$r["nom_contact"] .'</td>
       <td style="border: 1px solid black;">'.$r["mail_contact"] .'</td>
     </tr>';
  };
$table = $table .'</table>';
return $table;


}
add_shortcode( 'table', 'annuaire' );
